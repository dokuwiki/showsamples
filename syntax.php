<?php
  /**
   * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
   * @author     Francois <webmestre@fsl56.org>
   *
   * Plugin showSamples: display sample from plugins
   */
 
if (!defined ('DOKU_INC'))
  die ();
if (!defined ('DOKU_PLUGIN'))
  define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once (DOKU_PLUGIN.'syntax.php');

class syntax_plugin_showSamples extends DokuWiki_Syntax_Plugin {
 
  // ============================================================
  // function getInfo () {
  //   return confToHash (dirname (__FILE__).'/INFO.txt');
  // }
  function getType () { return 'substition'; }
  function getPType () { return 'block'; }
  function getSort () { return 299; }
  function getAllowedTypes() { return array ('container', 'baseonly', 'formatting', 'substition', 'protected', 'disabled', 'paragraphs'); }
  function connectTo ($mode) {
    $this->Lexer->addEntryPattern ('<showSamples[^>]*>', $mode, 'plugin_showsamples');
  }
  function postConnect() {
    $this->Lexer->addExitPattern ('</showSamples>', 'plugin_showsamples');
  }
 
  // ============================================================
  function handle ($match, $state, $pos, Doku_Handler $handler){
    switch ($state) {
    case DOKU_LEXER_ENTER:
      return array($state, trim (substr ($match, 12, -1))); // "<showSamples" => 12 ">" => 1
    case DOKU_LEXER_UNMATCHED:
      return array($state, $match);
    case DOKU_LEXER_EXIT:
      return array($state);
    }
    return false;
  }

  // ============================================================
  function render ($mode, Doku_Renderer $renderer, $indata) {
    if ($mode != 'xhtml')
      return false;
    if (empty ($indata))
      return false;
    list ($instr, $data) = $indata;
    //$data = " ".$data." ";
    $renderer->info['cache'] = FALSE;
    switch ($instr) {
    case DOKU_LEXER_ENTER :
      if (preg_match_all ("#(\"[^\"]*\")*help(\"[^\"]*\")*#", strtolower ($data), $dumy) > 0) {
	$this->help ($renderer);
	return true;
      }
      $imgDir = DOKU_REL.'lib/plugins/showsamples/images/';
      $renderer->doc .=
	'<div class="showSamples">'.NL.
	' <div class="navi">'.NL.
	'  <a onClick="showCarouselLeft()"><img src="'.$imgDir.'left.png" style="display:none;" class="leftside"/></a>'.NL.
	'  <a onClick="showCarouselRight()"><img src="'.$imgDir.'right.png" class="rightside"/></a>'.NL.
	' </div>'.NL.
	' <div class="slide">';
      break;
    case DOKU_LEXER_EXIT :
      $renderer->doc .=
	' </div>'.NL.
	'</div>';
      break;
    case DOKU_LEXER_UNMATCHED:
      $renderer->doc .= $renderer->_xmlEntities($data);
      break;
    }
    return true;
  }

  // ============================================================
  function help (&$renderer) {
    $url = "http://fsl56.org/admin/plugins/showSamples/";
    $renderer->doc .=
      '      <h1>Help showSamples V1.0</h1>'.NL.
      '      <ul>'.NL.
      '        <li><b>{{showSamples [help] <b>}}</b></li>'.NL.
      '      </ul>'.NL.
      '      <p><a class="urlextern" rel="nofollow" title="'.$url.'" href="'.$url.'">'.$url.'</a></p>'.NL;
  }

  // ============================================================
} // syntax_plugin_showSamples
?>
