/* DOKUWIKI:include_once jquery.scrollTo.min.js */

var showCarouselItem = 0;
var showCarouselDelai = JSINFO['carousel']['delai'];
var showCarouselMax = jQuery ('div.showSamples div.slide ul li').length;
var showCarouselTimeout;

function showCarouselLeft () {
    if (showCarouselItem < 1)
	return;
    showCarouselItem--;
    showCarouselMove ();
}

function showCarouselRight () {
    if (showCarouselItem >= showCarouselMax)
	return;
    showCarouselItem++;
    showCarouselMove ();
}

function showCarouselMove () {
    jQuery ("div.showSamples div.navi a img.leftside").each (function () {
	this.style.display = (showCarouselItem > 0) ? "" : "none";
    });
    var showCarouselMax = jQuery ('div.showSamples div.slide ul li').length;
    jQuery ("div.showSamples div.navi a img.rightside").each (function () {
	this.style.display = (showCarouselItem < (showCarouselMax-1)) ? "" : "none";
    });
    jQuery ("div.showSamples div.slide").scrollTo ('li:eq('+showCarouselItem+')', 1000);
}

function showCarousel () {
    showCarouselItem = 0;
    showCarouselMove ();
    showCarouselTimeout = setTimeout ("showCarouselNext ()", showCarouselDelai);
}

function showCarouselNext () {
    clearTimeout (showCarouselTimeout);
    if (showCarouselMax)
	showCarouselItem = (showCarouselItem+1) % (jQuery ('div.showSamples div.slide ul li').length);
    showCarouselMove ();
    showCarouselTimeout = setTimeout ("showCarouselNext ()", showCarouselDelai);
}

jQuery (showCarousel ());
