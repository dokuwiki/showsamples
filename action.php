<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <webmestre@fsl56.org>
 *
 * Plugin showSamples: display sample from plugins
 */
 
if (!defined ('DOKU_INC'))
    die ();
if (!defined ('DOKU_PLUGIN'))
    define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once (DOKU_PLUGIN.'action.php');

class action_plugin_showsamples extends DokuWiki_Action_Plugin {
 
    function register (Doku_Event_Handler $controller) {
        $controller->register_hook ('DOKUWIKI_STARTED', 'AFTER',  $this, '_defconf');
    }

    function _defconf (&$event, $param) {
        global $JSINFO;
        $JSINFO ['carousel'] = array ('delai' => ($this->getConf ('carouselDelai')*1000));
    }
}
