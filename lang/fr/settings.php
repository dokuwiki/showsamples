<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * French language file
 */

// for the configuration manager
$lang['carouselDelai']    = 'Délai de rotation du carrousel (en secondes)';
